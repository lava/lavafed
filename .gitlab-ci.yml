stages:
- test
- build
- deploy
- submit

#############
# Templates #
#############
.python:
  rules:
    - if: $CI_PIPELINE_SOURCE != "schedule"
  image:
    name: public.ecr.aws/lambda/python:3.11
    entrypoint:
    - "/usr/bin/env"
    - "PATH=/var/lang/bin:/usr/local/bin:/usr/bin/:/bin:/opt/bin"
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  cache:
    paths: [.cache/pip]
  before_script:
  - yum install --assumeyes make
  - python3 -V

.submit:
  extends: .python
  rules:
    - if: $CI_COMMIT_REF_NAME == "master" && $CI_PIPELINE_SOURCE == "schedule"
  stage: submit
  before_script: []
  script:
  - python3 -m pip install --upgrade -r requirements.txt
  - ./submit --url $LAVA_URL --username $LAVA_USER --token $LAVA_TOKEN --api-token $API_TOKEN $CI_ENVIRONMENT_NAME

.terraform:
  rules:
    - if: $CI_COMMIT_REF_NAME == "master" && $CI_PIPELINE_SOURCE != "schedule"
  image:
    name: hashicorp/terraform:1.5.5
    entrypoint:
    - "/usr/bin/env"
    - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  before_script:
  - terraform version
  - echo "credentials \"app.terraform.io\" {token = \"$TERRAFORM_TOKEN\"}" > $HOME/.terraformrc


########
# Test #
########
black:
  extends: .python
  stage: test
  script:
  - python3 -m pip install --upgrade black isort
  - make black

pylint:
  extends: .python
  stage: test
  script:
  - python3 -m pip install --upgrade pylint==2.17.5
  - make pylint

pytest:
  extends: .python
  stage: test
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  script:
  - python3 -m pip install --upgrade pytest pytest-cov pytest-django
  - python3 -m pip install --upgrade -r requirements.txt
  - python3 -m pip install --upgrade pysqlite3-binary
  - sed -i "s#from sqlite3 import dbapi2 as Database#from pysqlite3 import dbapi2 as Database#" /var/lang/lib/python3.11/site-packages/django/db/backends/sqlite3/base.py
  - make test
  artifacts:
    expire_in: 7 day
    paths:
    - htmlcov/
    reports:
      junit: lavafed.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

#########
# Build #
#########
build:
  extends: .python
  stage: build
  script:
  - yum install --assumeyes git
  - git fetch --prune --tags --unshallow
  - python3 -m pip install --upgrade lambpy
  - make build
  - md5sum lambda.zip
  artifacts:
    expire_in: 7 day
    paths:
    - lambda.zip

doc:
  extends: .python
  script:
  - python3 -m pip install --upgrade Sphinx==5.3.0
  - make -C doc html
  artifacts:
    paths:
      - doc/_build/html

##########
# Deploy #
##########
deploy:
  extends: .terraform
  stage: deploy
  variables:
    TF_WORKSPACE: prod
  environment:
    name: prod
    url: https://federation.lavasoftware.org
  resource_group: prod
  script:
  - migration_filepath=$(find lavafed/migrations/ -type f -name '[0-9]*_*.py' | sort | tail -1)
  - export TF_VAR_migration=$(basename "${migration_filepath}")
  - cd terraform/
  - terraform init
  - terraform validate
  - terraform apply -auto-approve

pages:
  rules:
    - if: $CI_PIPELINE_SOURCE != "schedule"
  stage: deploy
  dependencies:
    - doc
  script:
    - cp -a doc/_build/html public
  artifacts:
    paths:
      - public

publish-pages:
  rules:
    - if: $CI_PIPELINE_SOURCE != "schedule"
  stage: deploy
  needs:
    - pages
  trigger: lava/lava-docs
  variables:
    PARENT_PROJECT_NAME: $CI_PROJECT_NAME
    PARENT_PROJECT_REF: $CI_COMMIT_REF_NAME

#############
# Scheduled #
#############
submit-ledge:
  extends: .submit
  environment:
    name: ledge.validation.linaro.org
    url: https://ledge.validation.linaro.org

submit-lkft-staging:
  extends: .submit
  environment:
    name: lkft-staging.validation.linaro.org
    url: https://lkft-staging.validation.linaro.org

submit-morello:
  extends: .submit
  environment:
    name: lava.morello-project.org
    url: https://lava.morello-project.org

submit-staging:
  extends: .submit
  environment:
    name: staging.validation.linaro.org
    url: https://staging.validation.linaro.org

submit-tf:
  extends: .submit
  environment:
    name: tf.validation.linaro.org
    url: https://tf.validation.linaro.org
