from typing import List

from django.http import HttpResponseNotFound
from django.shortcuts import get_object_or_404
from ninja import Field, ModelSchema, NinjaAPI, Redoc, Schema
from ninja.pagination import RouterPaginated

from lavafed import models

api = NinjaAPI(
    title="LAVA Federation API",
    description="""
    LAVA Federation is a project aiming at testing the LAVA software on
    community owned hardware. The tests are spread across many labs with a
    variety of hardware.
    """,
    docs=Redoc(),
    openapi_extra={
        "info": {"contact": {"email": "support@lavacloud.io"}},
        "tags": [
            {"name": "Versions", "description": "Manage LAVA versions"},
        ],
    },
    default_router=RouterPaginated(),
    servers=[
        {
            "url": "https://federation.lavasoftware.org/",
            "description": "Production server",
        },
        {
            "url": "http://127.0.0.1:8000",
            "description": "Dev server",
        },
    ],
    version="0.2",
)


###########
# Schemas #
###########
class Error(Schema):
    detail: str


class Job(ModelSchema):
    device_type: str = Field(..., alias="dt.name")
    arch: str = Field(..., alias="get_arch_display")
    type: str = Field(..., alias="get_type_display")
    health: str = Field(..., alias="get_health_display")

    class Meta:
        model = models.Job
        fields = ["version", "name", "lab", "lava"]


class Version(ModelSchema):
    jobs: List[Job] = []

    class Meta:
        model = models.Version
        fields = ["version"]

    @staticmethod
    def resolve_jobs(obj):
        return obj.job_set.order_by("name", "lab__name")


#############
# Endpoints #
#############
@api.get(
    "/versions",
    response=List[Version],
    summary="List versions",
    tags=["Versions"],
)
def versions(request):
    versions = models.Version.objects.order_by("-version")
    return versions


@api.get(
    "/versions/{version_id}",
    response={200: Version, 404: Error},
    summary="Version details",
    tags=["Versions"],
)
def versions_details(request, version_id: str):
    if version_id == "latest":
        version = models.Version.objects.order_by("version").last()
        if version is None:
            return HttpResponseNotFound("There is no any versions yet.")
    else:
        version = get_object_or_404(models.Version, version=version_id)
    return version
