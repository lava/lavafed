from lavafed import __version__


def lavafed(request):
    return {
        "lavafed": {
            "version": __version__,
        }
    }
