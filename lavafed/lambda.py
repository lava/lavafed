import os

from django.core.asgi import get_asgi_application
from django.core.management import execute_from_command_line
from mangum import Mangum

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "lavafed.settings.prod")

application = get_asgi_application()

handler = Mangum(application, lifespan="off")


def db_handler(event, context):
    execute_from_command_line(["manage.py", "migrate"])
    return "OK"
