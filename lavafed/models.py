# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import random

from django.db import models


def _make_secret():
    # Set of valid characters for secret
    _SECRET_CHARS = "01234567890abcdefghijklmnopqrtsuwxyz"
    return "".join(random.SystemRandom().choice(_SECRET_CHARS) for i in range(64))


class DeviceType(models.Model):
    name = models.CharField(primary_key=True, max_length=200)

    def __str__(self):
        return "%s" % self.name


class Feature(models.Model):
    name = models.CharField(primary_key=True, max_length=200)
    description = models.TextField(null=True, blank=True)

    TYPE_GENERIC, TYPE_DEVICE = range(2)
    TYPE_CHOICES = ((TYPE_GENERIC, "Generic"), (TYPE_DEVICE, "Device"))
    type = models.IntegerField(choices=TYPE_CHOICES, default=TYPE_GENERIC)

    ACTION_NONE, ACTION_DEPLOY, ACTION_BOOT, ACTION_TEST = range(4)
    ACTION_CHOICES = (
        (ACTION_NONE, "None"),
        (ACTION_DEPLOY, "Deploy"),
        (ACTION_BOOT, "Boot"),
        (ACTION_TEST, "Test"),
    )
    action = models.IntegerField(choices=ACTION_CHOICES, default=ACTION_NONE)

    def __str__(self):
        return "%s" % self.name

    @classmethod
    def reverse_action(cls, action_str):
        action_str = action_str.lower()
        for a, s in cls.ACTION_CHOICES:
            if action_str == s.lower():
                return a
        return None

    @classmethod
    def reverse_type(cls, type_str):
        type_str = type_str.lower()
        for t, s in cls.TYPE_CHOICES:
            if type_str == s.lower():
                return t
        return None


class Job(models.Model):
    name = models.CharField(blank=False, max_length=255)
    dt = models.ForeignKey("DeviceType", on_delete=models.CASCADE)
    lab = models.ForeignKey("Lab", on_delete=models.CASCADE)
    version = models.ForeignKey("Version", on_delete=models.CASCADE)
    lava = models.BigIntegerField(default=0, verbose_name="Lava job id")
    url = models.URLField(default="")
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    features = models.ManyToManyField(Feature, blank=True)

    ARCH_AMD64, ARCH_AARCH64 = range(2)
    ARCH_CHOICES = ((ARCH_AMD64, "amd64"), (ARCH_AARCH64, "aarch64"))
    ARCH_REVERSE = {"amd64": ARCH_AMD64, "aarch64": ARCH_AARCH64}
    arch = models.IntegerField(choices=ARCH_CHOICES, default=ARCH_AMD64)

    TYPE_FED, TYPE_SLAVE, TYPE_META, TYPE_PACKAGE = range(4)
    TYPE_CHOICES = (
        (TYPE_FED, "FED"),
        (TYPE_SLAVE, "SLAVE"),
        (TYPE_META, "META"),
        (TYPE_PACKAGE, "PACKAGE"),
    )
    type = models.IntegerField(choices=TYPE_CHOICES, default=TYPE_FED)

    (
        HEALTH_UNKNOWN,
        HEALTH_COMPLETE,
        HEALTH_INCOMPLETE,
        HEALTH_CANCELED,
        HEALTH_MISMATCH,
    ) = range(5)
    HEALTH_CHOICES = (
        (HEALTH_UNKNOWN, "Unknown"),
        (HEALTH_COMPLETE, "Complete"),
        (HEALTH_INCOMPLETE, "Incomplete"),
        (HEALTH_CANCELED, "Canceled"),
        (HEALTH_MISMATCH, "Mismatch"),
    )
    HEALTH_REVERSE = {
        "Unknown": HEALTH_UNKNOWN,
        "Complete": HEALTH_COMPLETE,
        "Incomplete": HEALTH_INCOMPLETE,
        "Canceled": HEALTH_CANCELED,
        "Mismatch": HEALTH_MISMATCH,
    }
    health = models.IntegerField(choices=HEALTH_CHOICES, default=HEALTH_UNKNOWN)

    def __str__(self):
        return "%s (%s)" % (self.name, self.dt)


class Lab(models.Model):
    name = models.CharField(primary_key=True, max_length=200)
    url = models.URLField()

    def __str__(self):
        return "%s" % self.name


class Token(models.Model):
    token = models.CharField(max_length=64, default=_make_secret)
    description = models.CharField(blank=True, null=True, max_length=150)
    lab = models.ForeignKey(Lab, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("token", "lab")

    def __str__(self):
        return "%s - %s" % (self.lab.name, self.description)


class Version(models.Model):
    version = models.CharField(primary_key=True, max_length=200)

    def __str__(self):
        return "%s" % self.version
