# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import contextlib
import json

import svgwrite
import yaml
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Case, Count, IntegerField, Sum, When
from django.http import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseForbidden,
    HttpResponseNotFound,
    HttpResponseRedirect,
    JsonResponse,
)
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET, require_POST

from .models import DeviceType, Feature, Job, Lab, Token, Version


@csrf_exempt
@require_POST
def api_v0_1_jobs(request):
    # Check that a token does match
    token = request.META.get("HTTP_AUTHORIZATION")
    if not Token.objects.filter(token=token).exists():
        return HttpResponseForbidden("Missing token or unknown token")

    data = json.loads(request.body.decode("utf-8"))
    job_id = data["id"]
    job_def = yaml.safe_load(data["definition"])
    metadata = job_def["metadata"]

    # Find the right object
    lab, _ = Lab.objects.get_or_create(name=metadata["lab.name"])

    # Check that the token is right for this lab
    try:
        Token.objects.get(token=token, lab=lab)
    except Token.DoesNotExist:
        return HttpResponseForbidden("Wrong token")

    dt, _ = DeviceType.objects.get_or_create(name=metadata["device.type"])
    version, _ = Version.objects.get_or_create(version=metadata["lava.version"])
    job, _ = Job.objects.get_or_create(
        name=metadata["job.name"],
        dt=dt,
        lab=lab,
        version=version,
        arch=Job.ARCH_REVERSE[metadata["lava.arch"]],
    )
    job.lava = job_id
    job.url = metadata["job.url"].format(id=job_id)

    # Save the job type
    if metadata["job.type"] == "slave":
        job.type = Job.TYPE_SLAVE
    elif metadata["job.type"] == "meta":
        job.type = Job.TYPE_META
    elif metadata["job.type"] == "package":
        job.type = Job.TYPE_PACKAGE
    else:
        job.type = Job.TYPE_FED

    # Save the job health
    health_string = data["health_string"]
    log = data.get("log")
    if log:
        with contextlib.suppress(Exception):
            version_line = log.split("\n")[0]
            dispatcher_version = yaml.safe_load(version_line)[0]["msg"].split()[-1]
            if dispatcher_version != version.version:
                health_string = "Mismatch"
    job.health = Job.HEALTH_REVERSE[health_string]
    job.save()

    # Save the job features
    features_count = len(
        [
            k
            for k in metadata.keys()
            if k.startswith("features.") and k.endswith(".name")
        ]
    )
    for index in range(0, features_count):
        f_name = metadata["features." + str(index) + ".name"]
        f_type = metadata["features." + str(index) + ".type"]
        f_description = metadata["features." + str(index) + ".description"]
        f_action = metadata["features." + str(index) + ".action"]
        (feature, _) = Feature.objects.get_or_create(name=f_name)
        feature.description = f_description
        feature.type = Feature.reverse_type(f_type)
        if feature.type is None:
            return HttpResponseBadRequest("Invalid feature type")
        feature.action = Feature.reverse_action(f_action)
        if feature.action is None:
            return HttpResponseBadRequest("Invalid feature action")
        feature.save()
        job.features.add(feature)

    # TODO: save the job logs, results, ...

    return HttpResponse("ok")


@csrf_exempt
@require_GET
def api_v0_1_versions_show(request, version):
    if version == "latest":
        version = Version.objects.order_by("version").last()
        # last() returns None in case the queryset is empty.
        if version is None:
            return HttpResponseNotFound("No Version matches the given query.")
    else:
        version = get_object_or_404(Version, version=version)
    jobs = version.job_set.order_by("name", "lab__name", "name")
    data = [
        {
            "version": job.version_id,
            "name": job.name,
            "device_type": job.dt.name,
            "lab": job.lab.name,
            "lava": job.lava,
            "arch": job.get_arch_display(),
            "type": job.get_type_display(),
            "health": job.get_health_display(),
        }
        for job in jobs
    ]
    return JsonResponse(data, safe=False)


def home(request):
    return render(request, "lavafed/home.html", {})


def device_types(request):
    dts = DeviceType.objects.all().order_by("name")
    dts = dts.annotate(Count("job", distinct=True))
    dts = dts.annotate(Count("job__lab", distinct=True))
    return render(request, "lavafed/device_types.html", {"dts": dts})


def device_type_show(request, name):
    dt = get_object_or_404(DeviceType, name=name)
    data = (
        Job.objects.filter(dt=dt)
        .select_related("lab", "version")
        .order_by("-version__version")
    )
    paginator = Paginator(data, min(int(request.GET.get("per_pages", 20)), 100))
    try:
        jobs = paginator.page(request.GET.get("page"))
    except PageNotAnInteger:
        jobs = paginator.page(1)
    except EmptyPage:
        jobs = paginator.page(paginator.num_pages)

    return render(
        request,
        "lavafed/device_type_show.html",
        {
            "dt": dt,
            "jobs": jobs,
        },
    )


def labs(request):
    labs = Lab.objects.all().order_by("name")
    labs = labs.annotate(Count("job", distinct=True))
    labs = labs.annotate(Count("job__dt", distinct=True))
    return render(request, "lavafed/labs.html", {"labs": labs})


def lab_show(request, lab):
    lab = get_object_or_404(Lab, name=lab)
    data = (
        Job.objects.filter(lab=lab)
        .select_related("lab", "version")
        .order_by("-version__version", "name")
    )
    paginator = Paginator(data, min(int(request.GET.get("per_pages", 20)), 100))
    try:
        jobs = paginator.page(request.GET.get("page"))
    except PageNotAnInteger:
        jobs = paginator.page(1)
    except EmptyPage:
        jobs = paginator.page(paginator.num_pages)
    return render(request, "lavafed/lab_show.html", {"lab": lab, "jobs": jobs})


def versions(request):
    data = Version.objects.all().prefetch_related("job_set").order_by("-version")
    data = data.annotate(
        Count("job__dt", distinct=True),
        unknown=Sum(
            Case(
                When(job__health=Job.HEALTH_UNKNOWN, then=1),
                default=0,
                output_field=IntegerField(),
            )
        ),
        complete=Sum(
            Case(
                When(job__health=Job.HEALTH_COMPLETE, then=1),
                default=0,
                output_field=IntegerField(),
            )
        ),
        incomplete=Sum(
            Case(
                When(job__health=Job.HEALTH_INCOMPLETE, then=1),
                default=0,
                output_field=IntegerField(),
            )
        ),
        canceled=Sum(
            Case(
                When(job__health=Job.HEALTH_CANCELED, then=1),
                default=0,
                output_field=IntegerField(),
            )
        ),
        mismatch=Sum(
            Case(
                When(job__health=Job.HEALTH_MISMATCH, then=1),
                default=0,
                output_field=IntegerField(),
            )
        ),
    )
    paginator = Paginator(data, min(int(request.GET.get("per_pages", 20)), 100))
    try:
        versions = paginator.page(request.GET.get("page"))
    except PageNotAnInteger:
        versions = paginator.page(1)
    except EmptyPage:
        versions = paginator.page(paginator.num_pages)
    return render(request, "lavafed/versions.html", {"versions": versions})


def version_show(request, version):
    version = get_object_or_404(Version, version=version)
    jobs = version.job_set.select_related("dt", "lab").order_by(
        "dt__name", "lab__name", "name"
    )
    dts = (
        DeviceType.objects.filter(job__version=version, job__type=Job.TYPE_FED)
        .distinct()
        .order_by("name")
    )
    global_features = Feature.objects.filter(
        job__version=version, type=Feature.TYPE_GENERIC
    ).distinct()
    return render(
        request,
        "lavafed/version_show.html",
        {
            "dts": dts,
            "global_jobs": jobs.exclude(type=Job.TYPE_FED),
            "device_jobs": jobs.filter(type=Job.TYPE_FED),
            "global_features": global_features,
            "version": version,
        },
    )


def version_show_latest(request):
    version = Version.objects.order_by("version").last()
    if version:
        return HttpResponseRedirect(reverse("versions.show", args=[version.version]))
    else:
        return HttpResponseNotFound("No Version matches the given query.")


def version_badge(request, version):
    version = get_object_or_404(Version, version=version)
    jobs = version.job_set.all()
    jobs = jobs.aggregate(
        success=Sum(
            Case(
                When(health=Job.HEALTH_COMPLETE, then=1),
                default=0,
                output_field=IntegerField(),
            )
        ),
        failure=Sum(
            Case(
                When(
                    health__in=[
                        Job.HEALTH_UNKNOWN,
                        Job.HEALTH_INCOMPLETE,
                        Job.HEALTH_CANCELED,
                        Job.HEALTH_MISMATCH,
                    ],
                    then=1,
                ),
                default=0,
                output_field=IntegerField(),
            )
        ),
    )

    success = jobs["success"]
    failure = jobs["failure"]
    ratio = success / (success + failure) * 100
    title_text = version.version
    badge_text = "%d%%" % ratio
    if ratio == 100:
        badge_colour = "#5cb85c"
    elif ratio >= 80:
        badge_colour = "#f0ad4e"
    else:
        badge_colour = "#d9534f"

    font_size = 110
    character_width = font_size / 2
    padding_width = character_width
    title_width = len(title_text) * character_width + 2 * padding_width
    title_x = title_width / 2 + padding_width
    badge_width = len(badge_text) * character_width + 50
    badge_x = badge_width / 2 + 3 * padding_width + title_width
    total_width = (title_width + badge_width + 4 * padding_width) / 10

    dwg = svgwrite.Drawing("version_badge.svg", (total_width, 20))
    a = dwg.add(dwg.clipPath())
    a.add(dwg.rect(rx=3, size=(total_width, 20), fill="#fff"))
    b = dwg.add(dwg.linearGradient(end=(0, 1), id="b"))
    b.add_stop_color(0, "#bbb", 0.1)
    b.add_stop_color(1, None, 0.1)
    g1 = dwg.add(dwg.g(clip_path=a.get_funciri()))
    g1.add(
        dwg.path(
            fill="#555",
            d=[
                "M0",
                "0h",
                "%sv" % ((2 * padding_width + title_width) / 10),
                "20H",
                "0z",
            ],
        )
    )
    g1.add(
        dwg.path(
            fill=badge_colour,
            d=[
                "M%s" % ((2 * padding_width + title_width) / 10),
                "0h",
                "%sv" % ((2 * padding_width + badge_width) / 10),
                "20H",
                "%sz" % ((2 * padding_width + title_width) / 10),
            ],
        )
    )
    g1.add(
        dwg.path(fill=b.get_funciri(), d=["M0", "0h", "%sv" % total_width, "20H", "0z"])
    )

    g2 = dwg.add(
        dwg.g(
            fill="#fff",
            text_anchor="middle",
            font_family="monospace",
            font_size=font_size,
        )
    )
    g2.add(
        dwg.text(
            title_text,
            x=[title_x],
            y=[150],
            fill="#010101",
            fill_opacity=".3",
            transform="scale(.1)",
            textLength=title_width,
        )
    )
    g2.add(
        dwg.text(
            title_text,
            x=[title_x],
            y=[140],
            transform="scale(.1)",
            textLength=title_width,
        )
    )
    g2.add(
        dwg.text(
            badge_text,
            x=[badge_x],
            y=[150],
            fill="#010101",
            fill_opacity=".3",
            transform="scale(.1)",
            textLength=badge_width,
        )
    )
    g2.add(
        dwg.text(
            badge_text,
            x=[badge_x],
            y=[140],
            transform="scale(.1)",
            textLength=badge_width,
        )
    )
    badge = dwg.tostring()

    return HttpResponse(badge, content_type="image/svg+xml")


def version_badge_latest(request):
    try:
        version = Version.objects.order_by("version").last()
        return HttpResponseRedirect(reverse("versions.badge", args=[version.version]))
    except Version.DoesNotExist:
        return HttpResponseNotFound("No Version matches the given query.")
