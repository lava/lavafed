resource "aws_lambda_permission" "apigw" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.api.arn
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.api.execution_arn}/*/*"
}

resource "aws_lambda_function" "api" {
  function_name = "${local.workspace}-api"

  s3_bucket = aws_s3_bucket.lambda.id
  s3_key    = "lambda.zip"

  handler          = "lavafed.lambda.handler"
  runtime          = "python3.11"
  source_code_hash = filebase64sha256("${path.module}/../lambda.zip")
  memory_size      = 512
  timeout          = 35

  environment {
    variables = local.lambda_env
  }

  vpc_config {
    ipv6_allowed_for_dual_stack = true
    subnet_ids                  = [aws_subnet.subnet-a.id, aws_subnet.subnet-b.id]
    security_group_ids          = [aws_security_group.db.id]
  }

  role       = aws_iam_role.lambda_exec.arn
  depends_on = [aws_cloudwatch_log_group.api, aws_s3_object.lambda]

  tags = {
    Name = local.workspace
  }
}

resource "aws_lambda_function" "db" {
  function_name = "${local.workspace}-db"

  s3_bucket = aws_s3_bucket.lambda.id
  s3_key    = "lambda.zip"

  handler          = "lavafed.lambda.db_handler"
  runtime          = "python3.11"
  source_code_hash = filebase64sha256("${path.module}/../lambda.zip")
  memory_size      = 256
  timeout          = 35

  environment {
    variables = local.lambda_env
  }

  vpc_config {
    subnet_ids         = [aws_subnet.subnet-a.id, aws_subnet.subnet-b.id]
    security_group_ids = [aws_security_group.db.id]
  }

  role       = aws_iam_role.lambda_exec.arn
  depends_on = [aws_cloudwatch_log_group.api, aws_s3_object.lambda]

  tags = {
    Name = local.workspace
  }
}

resource "aws_lambda_invocation" "db" {
  depends_on    = [aws_lambda_function.db]
  function_name = aws_lambda_function.db.function_name

  input = jsonencode({
    migration = var.migration
  })

  lifecycle_scope = "CRUD"
}
