terraform {
  required_version = ">= 1.4.6"
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "lavasoftware"

    workspaces {
      prefix = "lavafed-"
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.11.0"
    }
  }
}
