resource "aws_db_subnet_group" "db" {
  name       = local.workspace
  subnet_ids = [aws_subnet.subnet-a.id, aws_subnet.subnet-b.id]

  tags = {
    Name = local.workspace
  }
}

resource "aws_db_instance" "db" {
  identifier = local.workspace

  engine         = "postgres"
  engine_version = "14"
  instance_class = "db.t4g.micro"

  allocated_storage     = 5
  max_allocated_storage = 10

  db_name  = "lavafed"
  username = "lavafed"
  password = random_password.database_password.result

  allow_major_version_upgrade = true
  auto_minor_version_upgrade  = true

  db_subnet_group_name   = aws_db_subnet_group.db.name
  vpc_security_group_ids = [aws_security_group.db.id]
  network_type           = "DUAL"

  backup_retention_period = 7
  copy_tags_to_snapshot   = true

  deletion_protection = true

  tags = {
    Name = local.workspace
  }
}
