data "aws_route53_zone" "api" {
  name = local.default.zone
}

resource "aws_route53_record" "api" {
  name    = aws_apigatewayv2_domain_name.api.domain_name
  type    = "A"
  zone_id = data.aws_route53_zone.api.zone_id

  alias {
    name                   = aws_apigatewayv2_domain_name.api.domain_name_configuration[0].target_domain_name
    zone_id                = aws_apigatewayv2_domain_name.api.domain_name_configuration[0].hosted_zone_id
    evaluate_target_health = false
  }
}

module "acm" {
  source      = "terraform-aws-modules/acm/aws"
  version     = "~> 4"
  domain_name = local.default.tld
  zone_id     = data.aws_route53_zone.api.zone_id

  validation_method = "DNS"
}
