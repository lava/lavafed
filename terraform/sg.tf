resource "aws_security_group" "db" {
  name   = "${local.workspace}-db"
  vpc_id = aws_vpc.vpc.id

  # Postgres uses port 5432
  ingress {
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    ipv6_cidr_blocks = [aws_vpc.vpc.ipv6_cidr_block]
    cidr_blocks      = [aws_vpc.vpc.cidr_block]
  }

  # Outbound internet access
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = local.workspace
  }
}
