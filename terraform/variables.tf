variable "region" {
  type    = string
  default = "us-east-1"
}

variable "sentry_dsn" {
  type    = string
  default = "https://c7d0e7a83dbe475f97f7cca93ace81f2@o371111.ingest.sentry.io/5413340"
}

variable "dev" {
  default = {
    tld  = "dev.federation.lavasoftware.org"
    zone = "dev.federation.lavasoftware.org."
  }
}

variable "prod" {
  default = {
    tld  = "federation.lavasoftware.org"
    zone = "federation.lavasoftware.org."
  }
}

variable "migration" {
  type        = string
  description = "Latest database migration file name."
  default     = ""
}

locals {
  default = terraform.workspace == "prod" ? var.prod : var.dev
  lambda_env = {
    DATABASE_URL = aws_ssm_parameter.database_url.value
    ENVIRONMENT  = terraform.workspace
    SENTRY_DSN   = var.sentry_dsn
    SECRET_KEY   = aws_ssm_parameter.django_secret_key.value
  }
  workspace = "lavafed-${terraform.workspace}"
}
