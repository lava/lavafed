resource "aws_vpc" "vpc" {
  cidr_block                       = "10.0.0.0/16"
  assign_generated_ipv6_cidr_block = true

  tags = {
    Name = local.workspace
  }
}

resource "aws_subnet" "subnet-a" {
  cidr_block        = "10.0.1.0/24"
  ipv6_cidr_block   = cidrsubnet(aws_vpc.vpc.ipv6_cidr_block, 8, 0)
  vpc_id            = aws_vpc.vpc.id
  availability_zone = "${var.region}a"

  assign_ipv6_address_on_creation = true

  tags = {
    Name = local.workspace
  }
}

resource "aws_subnet" "subnet-b" {
  cidr_block        = "10.0.2.0/24"
  ipv6_cidr_block   = cidrsubnet(aws_vpc.vpc.ipv6_cidr_block, 8, 1)
  vpc_id            = aws_vpc.vpc.id
  availability_zone = "${var.region}b"

  assign_ipv6_address_on_creation = true

  tags = {
    Name = local.workspace
  }
}
