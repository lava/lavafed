# vim: set ts=4

# Copyright 2019 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import os
import pathlib
import xmlrpc.client

import pytest

BASE_DIR = pathlib.Path(__file__).parent.parent.parent.absolute()
TEMPLATE_DIR = BASE_DIR / "etc" / "templates"


class RecordingProxyFactory:
    def __new__(cls):
        class RecordingProxy:
            master = None
            lab = None

            def __init__(self, uri, allow_none, transport):
                if (
                    uri
                    == "https://lava_master_user:lava_master_token@federation.lavasoftware.org/lava/RPC2/"
                ):
                    self.data = self.master
                elif uri == "https://lava_lab_user:lava_lab_token@lava.test/RPC2":
                    self.data = self.lab
                else:
                    assert 0  # nosec - pytest
                self.request = []

            def __del__(self):
                assert self.request == []  # nosec - pytest

            def __call__(self, *args):
                request = ".".join(self.request)
                self.request = []
                data = self.data.pop(0)
                assert request == data["request"]  # nosec - pytest
                if data.get("approx", False):
                    for a, b in zip(data["args"], args):
                        if isinstance(a, list):
                            for c in a:
                                assert c in b  # nosec - pytest
                        else:
                            assert a in b  # nosec - pytest
                else:
                    assert args == data["args"]  # nosec - pytest
                return data["ret"]

            def __getattr__(self, attr):
                self.request.append(attr)
                return self

        return RecordingProxy


@pytest.fixture
def setup(monkeypatch, tmpdir):
    data = """job_url: https://federation.lavasoftware.org/lava/scheduler/job/{id}
url: https://federation.lavasoftware.org/lava/RPC2/
username: lava_master_user
token: lava_master_token
api:
  url: https://federation.lavasoftware.org/api/v0.1/jobs/
  token: api_token
"""
    (tmpdir / "master.yaml").write_text(data, encoding="utf-8")

    data = """job_url: https://lava.test/scheduler/job/{id}
url: https://lava.test/RPC2
username: lava_lab_user
token: lava_lab_token
arch: aarch64
worker_token: my-token
slot:
  every: day
  starting: 01:00
  timezone: UTC
  duration: 3h
devices:
- name: device-01
- name: device-02
- name: device-03
  device-type: dt-lava-03
"""
    tmpdir.mkdir("labs")
    (tmpdir / "labs").mkdir("lava.test")
    (tmpdir / "labs" / "lava.test" / "config.yaml").write_text(data, encoding="utf-8")
    (tmpdir / "labs" / "lava.test").mkdir("jobs")
    data = '{% extends "lava-slave.jinja2" %}'
    (tmpdir / "labs" / "lava.test" / "jobs" / "lava-slave.jinja2").write_text(
        data, encoding="utf-8"
    )
    (tmpdir / "labs" / "lava.test" / "jobs").mkdir("device-01")
    data = '{% extends "health-checks/qemu.jinja2" %}'
    (tmpdir / "labs" / "lava.test" / "jobs" / "device-01" / "hc.jinja2").write_text(
        data, encoding="utf-8"
    )
    os.symlink(str(TEMPLATE_DIR), str(tmpdir / "templates"))
    monkeypatch.setattr(xmlrpc.client, "ServerProxy", RecordingProxyFactory())
