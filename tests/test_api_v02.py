import pytest
from django.urls import reverse

from lavafed.models import DeviceType, Job, Lab, Version


@pytest.fixture
def data(db):
    lab = Lab.objects.create(name="test-lab", url="http://exmple.com/")
    dt = DeviceType.objects.create(name="qemu")

    versions = ["2023.10.dev0051", "2023.10.dev0050"]
    for version in versions:
        v = Version.objects.create(version=version)
        Job.objects.create(
            version=v,
            name="qemu - hc",
            lab=lab,
            lava=versions.index(version),
            dt=dt,
        )


class TestAPI:
    def test_show_root(self, db, client):
        ret = client.get(reverse("api-0.2:openapi-view"))
        assert ret.status_code == 200

    def test_version_details_latest_404(self, db, client):
        ret = client.get(reverse("api-0.2:versions_details", args=["latest"]))
        assert ret.status_code == 404

    def test_version_details_latest(self, db, client, data):
        ret = client.get(reverse("api-0.2:versions_details", args=["latest"]))
        assert ret.status_code == 200
        assert ret.json()["version"] == "2023.10.dev0051"

    def test_version_details(self, db, client, data):
        ret = client.get(reverse("api-0.2:versions_details", args=["2023.10.dev0050"]))
        assert ret.status_code == 200
        assert ret.json() == {
            "jobs": [
                {
                    "device_type": "qemu",
                    "arch": "amd64",
                    "type": "FED",
                    "health": "Unknown",
                    "version": "2023.10.dev0050",
                    "name": "qemu - hc",
                    "lab": "test-lab",
                    "lava": 1,
                }
            ],
            "version": "2023.10.dev0050",
        }

    def test_versions(self, db, client, data):
        ret = client.get(reverse("api-0.2:versions"))
        assert ret.status_code == 200
        assert ret.json() == {
            "items": [
                {
                    "jobs": [
                        {
                            "device_type": "qemu",
                            "arch": "amd64",
                            "type": "FED",
                            "health": "Unknown",
                            "version": "2023.10.dev0051",
                            "name": "qemu - hc",
                            "lab": "test-lab",
                            "lava": 0,
                        }
                    ],
                    "version": "2023.10.dev0051",
                },
                {
                    "jobs": [
                        {
                            "device_type": "qemu",
                            "arch": "amd64",
                            "type": "FED",
                            "health": "Unknown",
                            "version": "2023.10.dev0050",
                            "name": "qemu - hc",
                            "lab": "test-lab",
                            "lava": 1,
                        }
                    ],
                    "version": "2023.10.dev0050",
                },
            ],
            "count": 2,
        }
