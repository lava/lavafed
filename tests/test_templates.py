# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import pathlib

import jinja2
import pytest
import yaml

from lavafed.utils import render_template

BASE_DIR = pathlib.Path(__file__).parent.parent.absolute()
TEMPLATE_DIR = BASE_DIR / "share" / "templates"


def test_raise_undefined():
    for f in TEMPLATE_DIR.glob("**/*.jinja2"):
        if f.name == "base.jinja2":
            continue
        with pytest.raises(jinja2.exceptions.UndefinedError):
            render_template(f, TEMPLATE_DIR)


def test_jobs():
    base_ctx = {
        "api_url": "https://federation.lavasoftware.org/api/v0.1/jobs/",
        "docker_version": "master-2018.10-0018-g08ea617a4",
        "job_timeout": 100,
        "job_url": "https://lava.com/scheduler/job/{id}",
        "lab_name": "lava.example.com",
        "arch": "amd64",
        "worker_token": "worker_token1",
        "version": "master-2018.10-0018-g08ea617a4",
        "timeout": 80,
    }

    for f in TEMPLATE_DIR.glob("**/*.jinja2"):
        if f.name == "base.jinja2":
            continue
        f_stem = f.stem
        if f.name in ["cubietruck-nfs.jinja2", "cubietruck-ramdisk.jinja2"]:
            f_stem = "cubietruck"
        elif f.name == "qemu-armv7.jinja2":
            f_stem = "qemu"

        ctx = base_ctx.copy()
        ctx["device_name"] = f_stem + "-01"
        data = yaml.safe_load(render_template(f, TEMPLATE_DIR, **ctx))

        assert data["device_type"] == f_stem  # nosec - pytest
        assert data["job_name"].startswith(  # nosec - pytest
            "[lavafed master-2018.10-0018-g08ea617a4] " + f_stem
        )
        assert data["timeouts"]["job"] == {"seconds": 100}  # nosec - pytest
        assert data["visibility"] == "public"  # nosec - pytest

        assert data["metadata"]["device.type"] == f_stem  # nosec - pytest
        assert data["metadata"]["job.type"] == "test"  # nosec - pytest
        assert data["metadata"]["lab.name"] == "lava.example.com"  # nosec - pytest
        assert data["metadata"]["lava.arch"] == "amd64"  # nosec - pytest
        assert (  # nosec - pytest
            data["metadata"]["lava.version"] == "master-2018.10-0018-g08ea617a4"
        )

        assert data["notify"] == {  # nosec - pytest
            "criteria": {"status": "finished"},
            "callback": {
                "url": "https://federation.lavasoftware.org/api/v0.1/jobs/",
                "token": "LAVAFED_TOKEN",
                "method": "POST",
                "dataset": "all",
                "content-type": "json",
            },
        }
