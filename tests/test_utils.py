# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

from lavafed.utils import build_uri


def test_build_uri():
    assert build_uri("https://example.com/RPC2", "user", "token") == (  # nosec - pytest
        "https",
        "https://user:token@example.com/RPC2",
    )
    assert build_uri("http://example.com/RPC2", "user", "token") == (  # nosec - pytest
        "http",
        "http://user:token@example.com/RPC2",
    )
    assert build_uri(  # nosec - pytest
        "https://example.com:8443/RPC2", "user", "token"
    ) == ("https", "https://user:token@example.com:8443/RPC2")
