# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import json

import pytest
import yaml
from django.urls import reverse

from lavafed.models import DeviceType, Feature, Job, Lab, Token, Version


@pytest.mark.django_db
def test_post_test(client):
    # Does not accept GET
    ret = client.get(reverse("api.v0.1.jobs"))
    assert ret.status_code == 405  # nosec - pytest

    lab = Lab.objects.create(name="example.com")
    token = Token.objects.create(lab=lab)

    # Test a basic POST request
    assert DeviceType.objects.count() == 0  # nosec - pytest
    assert Job.objects.count() == 0  # nosec - pytest
    assert Lab.objects.count() == 1  # nosec - pytest
    assert Version.objects.count() == 0  # nosec - pytest
    job_def = {
        "metadata": {
            "lab.name": "example.com",
            "device.type": "qemu-aarch64",
            "job.url": "https://lava.com/scheduler/job/{id}",
            "job.name": "qemu - health-check",
            "job.type": "test",
            "lava.version": "2018.10.0040.gac0097c42",
            "lava.arch": "aarch64",
            "features.0.name": "health-check",
            "features.0.type": "generic",
            "features.0.description": "Device is able to boot",
            "features.0.action": "None",
            "features.1.name": "deploy.tftp",
            "features.1.type": "device",
            "features.1.description": "Deploy on tftp",
            "features.1.action": "deploy",
        }
    }
    data = {
        "id": 4212,
        "definition": yaml.safe_dump(job_def),
        "health_string": "Complete",
    }
    ret = client.post(
        reverse("api.v0.1.jobs"),
        content_type="application/json",
        data=json.dumps(data),
        HTTP_AUTHORIZATION=token.token,
    )
    assert ret.status_code == 200  # nosec - pytest

    assert DeviceType.objects.count() == 1  # nosec - pytest
    assert DeviceType.objects.last().name == "qemu-aarch64"  # nosec - pytest
    assert Job.objects.count() == 1  # nosec - pytest
    job = Job.objects.last()
    assert job.lab == Lab.objects.first()  # nosec - pytest
    assert job.lava == 4212  # nosec - pytest
    assert job.name == "qemu - health-check"  # nosec - pytest
    assert job.url == "https://lava.com/scheduler/job/4212"  # nosec - pytest
    assert job.version.version == "2018.10.0040.gac0097c42"  # nosec - pytest
    assert job.arch == Job.ARCH_AARCH64  # nosec - pytest
    assert Lab.objects.count() == 1  # nosec - pytest
    assert Lab.objects.last().name == "example.com"  # nosec - pytest
    assert Version.objects.count() == 1  # nosec - pytest
    assert Version.objects.last().version == "2018.10.0040.gac0097c42"  # nosec - pytest
    assert Feature.objects.count() == 2  # nosec - pytest
    assert (  # nosec - pytest
        Feature.objects.get(name="health-check", type=0).description
        == "Device is able to boot"
    )
    assert (  # nosec - pytest
        Feature.objects.get(name="deploy.tftp", type=1).description == "Deploy on tftp"
    )
    assert job.health == job.HEALTH_COMPLETE

    # Test dispatcher version mismatches.
    log = '- {"dt": "2023-06-29T08:48:44.211468", "lvl": "info", "msg": "lava-dispatcher, installed at version: 2023.05.1.0110.g216d56fac"}\n...'
    data["log"] = log
    ret = client.post(
        reverse("api.v0.1.jobs"),
        content_type="application/json",
        data=json.dumps(data),
        HTTP_AUTHORIZATION=token.token,
    )
    assert ret.status_code == 200
    job = Job.objects.last()
    assert job.health == job.HEALTH_MISMATCH

    # Test dispatcher version matches.
    log = '- {"dt": "2023-06-29T08:48:44.211468", "lvl": "info", "msg": "lava-dispatcher, installed at version: 2018.10.0040.gac0097c42"}\n...'
    data["log"] = log
    ret = client.post(
        reverse("api.v0.1.jobs"),
        content_type="application/json",
        data=json.dumps(data),
        HTTP_AUTHORIZATION=token.token,
    )
    assert ret.status_code == 200
    job = Job.objects.last()
    assert job.health == job.HEALTH_COMPLETE
